# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower_letter = 0
    upper_letter = 0
    digit = 0
    character = 0
    for item in password:
        if item == "$" or item == "!" or item == "@":
            character += 1
        elif item.isdigit():
            digit += 1
        elif item.islower():
            lower_letter += 1
        elif item.isupper():
            upper_letter += 1
        else:
            return False
    if character > 0 and digit > 0 and lower_letter > 0 and upper_letter > 0 and len(password) >= 6 and len(password) <= 12:
        return True
    else:
        return False                          
